package com.example.papb075087223

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

class PengaturanFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_pengaturan, container, false)

        val tvAnggotaKelompok = view.findViewById<TextView>(R.id.tv_anggkota_kelompok)

        tvAnggotaKelompok.setOnClickListener {
            startActivity(Intent(context, KelompokActivity::class.java))
        }

        return view
    }
}