package com.example.papb075087223

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

class HomeFragment : Fragment() {
    private lateinit var vibrator: Vibrator

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        val ivToggle = view.findViewById<ImageView>(R.id.iv_toggle)
        val tvToggle = view.findViewById<TextView>(R.id.tv_toggle)

        vibrator = (context?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator)
        var isOn = true

        ivToggle.setOnClickListener {
            if (isOn) {
                ivToggle.setImageResource(R.drawable.icon_off)
                tvToggle.text = "OFF"
                isOn = false
                vibrate()
            } else {
                ivToggle.setImageResource(R.drawable.icon_on)
                tvToggle.text = "ON"
                isOn = true
                vibrate()
            }
        }

        return view
    }

    fun vibrate() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            vibrator.cancel()
            vibrator.vibrate(VibrationEffect.createPredefined(VibrationEffect.EFFECT_CLICK))
        } else {
            vibrator.vibrate(50)
        }
    }
}